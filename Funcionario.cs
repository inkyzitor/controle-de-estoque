using System.Collections.Generic;

namespace dotnetcore
{
    public class Funcionario
    {
        private string nomeFuncionario;
        private int codiogFuncionario;

        public List<Funcionario> Funcionarios = new List<Funcionario>();

        public Funcionario(string nomeFuncionario, int codiogFuncionario)
        {
            this.nomeFuncionario = nomeFuncionario;
            this.codiogFuncionario = codiogFuncionario;
        }
        
        
    }
}