using System;

namespace dotnetcore
{
    public class Chamar
    {
        public void chamadaPorId()
        {
            Console.Write("ID do produto: ");
            int id = Convert.ToInt16(Console.ReadLine());
            
            if (id != Produtos.id)
            {
                Console.WriteLine("\n ID invalido");
                Console.WriteLine();
                chamadaPorId();
            }
            Produtos encontrado = Produtos.Buscar(id);
            Console.WriteLine(encontrado);
            
        }

    }
}