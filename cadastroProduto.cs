using System;

namespace dotnetcore
{
    public class CadastroProduto : ICadastro
    {
        void ICadastro.FazerCadastro()
        {
            FazerCadastro();
        }

        string ICadastro.Cadastrar()
        {
            return Cadastrar();
        }
        
        public void FazerCadastro()
        {
            string resposta;
            do
            {
                Console.WriteLine("cadastrar proximo? (S/N)");
                resposta = Convert.ToString(Console.ReadLine());
            } while (resposta == "S" || resposta == "s");
            Console.WriteLine("-----------------------");
        }


        private string Cadastrar()
        {
           Console.Write("Digite o nome: ");
           string nome = Console.ReadLine();

           Console.Write("Digite o valor: ");
           double valor = Convert.ToDouble(Console.ReadLine());

           Console.Write("quantidade: ");
           int quantidade = Convert.ToInt16(Console.ReadLine());

           Produtos produto = new Produtos(nome, valor, quantidade);
           
           return "produto cadastrado";
        }

    }
}